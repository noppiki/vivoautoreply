import datetime
import json
import os
import re
import sys
import threading
import time
import tkinter

from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait

USER_ID = '335685'
PASSWORD = '0330'
TEST_ID = '88607'
SLEEP_TIME = 120

is_dummy = True


def erase_sent_history():
    print("erase_history")
    json.dump({}, open('sent_message_id.json', 'w'))


def login():
    user_agent = 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_2 like Mac OS X) AppleWebKit/603.2.4 (KHTML, like Gecko) ' \
                 'Version/10.0 Mobile/14F89 Safari/602.1 '
    # PhantomJS本体のパス
    if os.name == 'posix':
        pjs_path = '/usr/local/bin/phantomjs'
    else:
        pjs_path = os.path.dirname(sys.argv[0]) + '/phantomjs'

    dcap = {
        "phantomjs.page.settings.userAgent": user_agent,
        'marionette': True
    }
    driver = webdriver.PhantomJS(executable_path=pjs_path, desired_capabilities=dcap)
    # 5秒待機
    wait = WebDriverWait(driver, 5)
    driver.get("http://vi-vo.link/module/login.php")
    driver.find_element_by_xpath('//*[@id="username_girl"]').send_keys(USER_ID)
    driver.find_element_by_xpath('//*[@id="password_girl"]').send_keys(PASSWORD)
    driver.find_element_by_class_name('loginpage-btn').click()
    return driver


def get_history(browser):
    print("足跡のチェックを開始します")
    browser.find_element_by_xpath('//*[@id="tabbar"]/ul/li[2]/a/div/i').click()
    messages = browser.find_elements_by_class_name('message')
    print(str(len(messages)) + "件の足跡を取得しました")
    footprints = {}
    for message in messages:
        anchor = message.find_element_by_tag_name('a')
        href = anchor.get_attribute('href')
        match = re.search(r'訪問日時：(.+)\n', anchor.text)
        if match:
            access_time = match.group(1)
        match = re.search(r'char_id=(.+)', href)
        if match:
            user_id = match.group(1)
        footprints.update({user_id: access_time})
        print("新規足跡:" + user_id + ":" + access_time)

    f = open('footprint.json', 'w')
    json.dump(footprints, f)
    f.close()


def get_is_favorited(browser):
    print("お気に入りされた状況をチェックします")
    last_favorited = json.load(open('favorited.json', 'r'))
    json.dump(last_favorited, open('last_favorited.json', 'w'))
    browser.save_screenshot("ss.png")
    browser.get("http://vi-vo.link/module/favorite_user_list.php")
    time.sleep(2)
    messages = browser.find_elements_by_class_name('message')
    print(str(len(messages)) + "件のお気に入りがあります")
    favorited_id = []
    for message in messages:
        anchor = message.find_element_by_tag_name('a')
        href = anchor.get_attribute('href')
        match = re.search(r'char_id=(.+)', href)
        if match:
            favorited_id.append(match.group(1))
    last_set = set(last_favorited.keys())
    current_set = set(favorited_id)
    diff_set = (current_set - last_set)
    for diff in diff_set:
        last_favorited.update({diff: datetime.datetime.now().strftime("%Y/%m/%d %H:%M")})
    f = open('favorited.json', 'w')
    json.dump(last_favorited, f)
    f.close()
    print("お気に入りチェック完了")


def check_new_favorited():
    favorited = json.load(open('favorited.json', 'r'))
    footprint = json.load(open('footprint.json', 'r'))
    favorited_set = set(favorited.keys())
    footprint_set = set(footprint.keys())
    matched_list = list(favorited_set & footprint_set)
    for m in matched_list:
        print("新規お気に入り" + str(m))
    return matched_list


def check_favorited_time(favorited_id):
    favorited = json.load(open('favorited.json', 'r'))
    footprint = json.load(open('footprint.json', 'r'))
    id_list = []
    for id in favorited_id:
        favorited_time = datetime.datetime.strptime(favorited[id], '%Y/%m/%d %H:%M')
        footprint_time = datetime.datetime.strptime(footprint[id], '%Y/%m/%d %H:%M')
        if (favorited_time - footprint_time) < datetime.timedelta(minutes=4):
            id_list.append(id)
    return id_list


def check_sent_message(ids):
    sent_ids = json.load(open('sent_message_id.json', 'r'))
    unsent = []
    for id in ids:
        if id in sent_ids.keys():
            unsent.append(id)
    return unsent


def get_favorite_message():
    return open('favorite_message.txt', 'r').read()


def save_sent_message(sent_id, type):
    sent_ids = json.load(open('sent_message_id.json', 'r'))
    sent_ids.update({sent_id: type})
    json.dump(sent_ids, open('sent_message_id.json', 'w'))


def get_footprint_message():
    return open('footprint_message.txt', 'r', encoding='utf-8').read()


def get_reward_message():
    return open('reward_message.txt', 'r', encoding='utf-8').read()


def get_special_message():
    return open('special_message.txt', 'r', encoding='utf-8').read()


def get_waiting_message():
    return open('waiting_message.txt', 'r', encoding='utf-8').read()


def send_message(ids, browser, type):
    print("メッセージ送信中")
    attach = ""
    if type == "favorite":
        message_all = get_favorite_message()
    elif type == "footprint":
        message_all = get_footprint_message()
    elif type == "reward":
        message_all = get_reward_message()
    elif type == "special":
        message_all = get_special_message()
    elif type == "waiting":
        message_all = get_waiting_message()

    s = message_all.split("<")
    message = s[0]
    print("送信対象" + str(len(ids)) + "件")
    for id in ids:
        print(str(id) + "にメッセージ「" + message + "」を送信中")
        message_url = "http://vi-vo.link/module/message_detail.php?char_id2={}".format(id)
        browser.get(message_url)
        browser.find_element_by_xpath('//*[@id="mess"]').send_keys(message)
        if len(s) > 1:
            attach = s[1]
            match = re.search(r'attach=(.+)>', attach)
            if match:
                is_remote = browser._is_remote
                browser._is_remote = False
                print(match.group(1))
                browser.find_element_by_class_name('pdgl10').send_keys(
                    match.group(1))
                browser._is_remote = is_remote
        if is_dummy:
            if not check_already_sent_footprint(id, type):
                print(browser.find_element_by_class_name('message-title-name').text + "に \n" + message + "と送信しました")
                browser.save_screenshot(id + ".png")
        else:
            if not check_already_sent_footprint(id, type):
                browser.save_screenshot('ss.png')
                browser.find_element_by_class_name('send-submit').submit()
        length = len(s)

        if len(s) > 2:
            for i in range(2, length):
                attach = s[i]
                match = re.search(r'attach=(.+)>', attach)
                if match:
                    is_remote = browser._is_remote
                    browser._is_remote = False
                    print(match.group(1))
                    browser.find_element_by_class_name('pdgl10').send_keys(
                        match.group(1))
                    browser._is_remote = is_remote
                    if is_dummy:
                        if not check_already_sent_footprint(id, type):
                            print(browser.find_element_by_class_name(
                                'message-title-name').text + "に \n" + message + "と送信しました")
                    else:
                        if not check_already_sent_footprint(id, type):
                            browser.save_screenshot('ss.png')
                            browser.find_element_by_class_name('send-submit').submit()

        save_sent_message(id, type)


def check_favorited(browser, footprints):
    unfavorite = []
    for footprint in footprints:
        browser.get("http://vi-vo.link/module/prf_detail.php?char_id={}".format(footprint))
        if not "気に入られ中" in browser.find_element_by_xpath('/html/body/div[3]/div[1]/table[2]').get_attribute(
                'innerHTML'):
            unfavorite.append(footprint)
        else:
            print(str(footprint) + "はお気に入り中")
    return unfavorite


def check_sent_footprint(ids):
    print(str(len(ids)) + "件の送信候補")
    sent_ids = json.load(open('sent_message_id.json', 'r'))
    ids_set = set(ids)
    sent_set = set(sent_ids.keys())
    return list(ids_set - sent_set)


def send_footprint_message(browser, waiting_mode):
    print("足跡をつけた人にメッセージを送ります")
    footprints = json.load(open('footprint.json', 'r'))
    print("足跡対象" + str(len(footprints)) + "件")
    un_favoriteds = check_favorited(browser, footprints)
    print("お気に入りにしていない人" + str(len(un_favoriteds)) + "件")
    un_sent = check_sent_footprint(un_favoriteds)
    print("足跡メッセージ送信対象" + str(len(un_favoriteds)) + "件")
    if waiting_mode:
        send_message(un_sent, browser, "waiting")
    else:
        send_message(un_sent, browser, "footprint")


def get_footprint_sent_user():
    user_data = json.load(open('sent_message_id.json', 'r'))
    return [k for k, v in user_data.items() if v == "footprint"]


def check_already_sent_footprint(id, type):
    users = json.load(open('sent_message_id.json', 'r'))
    if id in users.keys():
        if users[id] == type:
            return True
    return False

def get_keyword():
    return open('reward_keyword.txt', 'r', encoding='utf-8').read()


def get_special_keyword():
    return open('special_keyword.txt', 'r', encoding='utf-8').read()


def check_sent_user_message(browser):
    users = get_footprint_sent_user()
    for user in users:
        url = 'http://vi-vo.link/module/message_detail.php?char_id2={}'.format(user)
        browser.get(url)
        questions = browser.find_elements_by_class_name('arrow_question')
        if len(questions) > 1:
            for question in questions:
                print(question.text)
                if get_keyword() in question.text:
                    send_message([user], browser, "reward")


def check_reward_sent_user_message(browser):
    user_data = json.load(open('sent_message_id.json', 'r'))
    users = [k for k, v in user_data.items() if v == "reward"]
    for user in users:
        url = 'http://vi-vo.link/module/message_detail.php?char_id2={}'.format(user)
        browser.get(url)
        questions = browser.find_elements_by_class_name('question_date')
        answers = browser.find_elements_by_class_name('answer_date')
        last_answer = answers[-1].text.split('\n')
        last_question = questions[-1].text.split('\n')
        if len(answers) >= 2:
            if datetime.datetime.strptime(last_answer[0] + " " + last_answer[1],
                                          '%m/%d %H:%M') < datetime.datetime.strptime(
                                last_question[0] + " " + last_question[1], '%m/%d %H:%M'):
                if get_special_keyword() in answers[-1].text:
                    send_message([user], browser, "special")


def auto_pilot():
    global waiting_value
    if waiting_value.get():
        SLEEP_TIME = 60
    else:
        SLEEP_TIME = 120
    driver = login()
    print(datetime.datetime.now().strftime("%H:%M") + " 巡回開始")
    get_history(driver)
    get_is_favorited(driver)
    new_favorited = check_new_favorited()
    favorite_ids = check_favorited_time(new_favorited)

    if len(favorite_ids) > 0:
        unsent_ids = check_sent_message(favorite_ids)
        if len(unsent_ids) > 0:
            send_message(unsent_ids, driver, "favorite")

    send_footprint_message(driver, waiting_value.get())
    check_sent_user_message(driver)
    check_reward_sent_user_message(driver)
    driver.quit()
    time.sleep(SLEEP_TIME)
    th = threading.Thread(target=auto_pilot, name="th")
    th.setDaemon(True)
    th.start()


def start_check():
    global is_dummy
    global erase_flag
    is_dummy = dummy_value.get()
    if erase_flag.get():
        erase_sent_history()
    th = threading.Thread(target=auto_pilot, name="th")
    th.setDaemon(True)
    th.start()


root = tkinter.Tk()
root.title(u"vi-vo自動巡回")
dummy_value = tkinter.BooleanVar()
dummy_value.set(True)
dummy_button = tkinter.Checkbutton(text=u"ダミー送信", variable=dummy_value)
dummy_button.pack()
erase_flag = tkinter.BooleanVar()
erase_flag.set(False)
erase_button = tkinter.Checkbutton(text=u"送信履歴削除", variable=erase_flag)
erase_button.pack()
waiting_value = tkinter.BooleanVar()
waiting_value.set(False)
waiting_button = tkinter.Checkbutton(text=u"待機中モード", variable=waiting_value)
waiting_button.pack()
button1 = tkinter.Button(root, text=u'開始', command=lambda: start_check())
button1.pack()
button2 = tkinter.Button(text=u'終了', command=lambda: sys.exit())
button2.pack()
root.mainloop()
